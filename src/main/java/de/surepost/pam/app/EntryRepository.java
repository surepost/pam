package de.surepost.pam.app;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EntryRepository extends JpaRepository<EntryEntity, Long> {
    @Query("SELECT e FROM EntryEntity e WHERE " +
            "LOWER(e.zip) LIKE LOWER(CONCAT('%', :keyword, '%')) OR " +
            "LOWER(e.city) LIKE LOWER(CONCAT('%', :keyword, '%')) OR " +
            "LOWER(e.street) LIKE LOWER(CONCAT('%', :keyword, '%')) OR " +
            "LOWER(e.house) LIKE LOWER(CONCAT('%', :keyword, '%'))")
    Page<EntryEntity> findAll(String keyword, Pageable pageable);
}
