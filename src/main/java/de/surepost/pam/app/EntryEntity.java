package de.surepost.pam.app;

import de.surepost.pam.Address;
import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name = "entries")
public class EntryEntity implements Address {

    @Id
    @Column(nullable = false)
    public Long id;

    @Column(nullable = false)
    public String zip;

    @Column(nullable = false)
    public String city;

    @Column(nullable = false)
    public String street;

    @Column()
    public String house;

    @Column()
    public String phone;

    @Column()
    public String email;

    @Column(name = "verified")
    public Boolean exists;

    @OneToMany(mappedBy = "entry")
    private Set<EntryEventEntity> events;

    public EntryEntity() {
    }

    @Override
    public String zip() {
        return zip;
    }

    @Override
    public String city() {
        return city;
    }

    @Override
    public String street() {
        return street;
    }

    @Override
    public String house() {
        return house;
    }
}