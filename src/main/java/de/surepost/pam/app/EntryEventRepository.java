package de.surepost.pam.app;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EntryEventRepository extends JpaRepository<EntryEventEntity, Long> {
    @Query("SELECT e FROM EntryEventEntity e WHERE e.entry.id = :entryId")
    Page<EntryEventEntity> findAll(Long entryId, Pageable pageable);

    @Query("SELECT e FROM EntryEventEntity e WHERE " +
            "(LOWER(e.event) LIKE LOWER(CONCAT('%', :keyword, '%')) OR " +
            "LOWER(e.username) LIKE LOWER(CONCAT('%', :keyword, '%'))) AND e.entry.id = :entryId")
    Page<EntryEventEntity> findAll(String keyword, Long entryId, Pageable pageable);
}
