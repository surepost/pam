package de.surepost.pam.app;

import de.surepost.pam.InputCommand;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
class AppScheduler {
    private final InputCommand inputCommand;

    public AppScheduler(InputCommand inputCommand) {
        this.inputCommand = inputCommand;
    }

    @Scheduled(fixedDelay = 1_000)
    public void poll() {
        inputCommand.execute();
    }
}
