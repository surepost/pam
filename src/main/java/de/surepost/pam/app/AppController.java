package de.surepost.pam.app;

import de.surepost.pam.OutputCommand;
import de.surepost.pam.api.ApiServiceException;
import de.surepost.pam.api.ApiServiceInterface;
import de.surepost.pam.api.ApiVerifyRequest;
import de.surepost.pam.api.ApiVerifyResponse;
import de.surepost.pam.output.OutputRecord;
import de.surepost.pam.output.OutputServiceException;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
class AppController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppController.class);
    private final EntryRepository entryRepository;
    private final EntryEventRepository entryEventRepository;
    private final ApiServiceInterface apiService;
    private final OutputCommand exportCommand;

    public AppController(
            EntryRepository entryRepository,
            EntryEventRepository entryEventRepository,
            ApiServiceInterface apiService,
            OutputCommand exportCommand
    ) {
        this.entryRepository = entryRepository;
        this.entryEventRepository = entryEventRepository;
        this.apiService = apiService;
        this.exportCommand = exportCommand;
    }

    private static Pageable getPageable(HttpServletRequest request) {
        int start = Integer.parseInt(request.getParameter("start"));
        int length = Integer.parseInt(request.getParameter("length"));
        int sortColumnIndex = Integer.parseInt(request.getParameter("order[0][column]"));
        String sortDirection = request.getParameter("order[0][dir]");
        String sortColumnName = request.getParameter("columns[" + sortColumnIndex + "][data]");
        return PageRequest.of(start / length, length, Sort.by(Sort.Direction.fromString(sortDirection), sortColumnName));
    }

    @PostMapping(value = "/verify")
    public ResponseEntity<ApiVerifyResponse> verify(@RequestBody ApiVerifyRequest request) {
        try {
            return ResponseEntity.ok(apiService.verify(request));
        } catch (ApiServiceException e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PostMapping(value = "/edit")
    public ResponseEntity<EditResponse> edit(@RequestBody OutputRecord request) {
        try {
            exportCommand.execute(request, SecurityContextHolder.getContext().getAuthentication().getName());
            LOGGER.info(String.format("Exported record: %d", request.id()));
            return ResponseEntity.ok(new EditResponse(request.id()));
        } catch (OutputServiceException | EntryNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PostMapping(value = "/entries")
    public EntriesResponse entries(HttpServletRequest request) {
        int draw = Integer.parseInt(request.getParameter("draw"));
        String keyword = request.getParameter("search[value]");
        Pageable pageable = getPageable(request);
        Page<EntryEntity> page = performSearch(keyword, pageable, entryRepository::findAll);
        return new EntriesResponse(draw, page.getTotalElements(), page.getTotalElements(), page.getContent());
    }

    @PostMapping(value = "/events/{entryId}")
    public EventsResponse events(@PathVariable Long entryId, HttpServletRequest request) {
        int draw = Integer.parseInt(request.getParameter("draw"));
        String keyword = request.getParameter("search[value]");
        Pageable pageable = getPageable(request);
        Page<EntryEventEntity> page = performSearch(entryId, keyword, pageable, entryEventRepository::findAll);
        return new EventsResponse(draw, page.getTotalElements(), page.getTotalElements(), page.getContent());
    }

    private <T> Page<T> performSearch(String keyword, Pageable pageable, SearchFunction<T> searchFunction) {
        return keyword.isEmpty() ? searchFunction.search(pageable) : searchFunction.search(keyword, pageable);
    }

    private <T> Page<T> performSearch(Long entryId, String keyword, Pageable pageable, SearchFunctionWithId<T> searchFunction) {
        return keyword.isEmpty() ? searchFunction.search(entryId, pageable) : searchFunction.search(keyword, entryId, pageable);
    }

    @FunctionalInterface
    private interface SearchFunction<T> {
        Page<T> search(String keyword, Pageable pageable);

        default Page<T> search(Pageable pageable) {
            return search("", pageable);
        }
    }

    @FunctionalInterface
    private interface SearchFunctionWithId<T> {
        Page<T> search(String keyword, Long entryId, Pageable pageable);

        default Page<T> search(Long entryId, Pageable pageable) {
            return search("", entryId, pageable);
        }
    }

    record EditResponse(Long id) {
    }

    record EntriesResponse(long draw, long recordsTotal, long recordsFiltered, List<EntryEntity> data) {
    }

    record EventsResponse(long draw, long recordsTotal, long recordsFiltered, List<EntryEventEntity> data) {
    }
}
