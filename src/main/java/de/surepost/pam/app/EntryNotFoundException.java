package de.surepost.pam.app;

import java.io.IOException;

public class EntryNotFoundException extends IOException {
    public EntryNotFoundException(Long context) {
        super(String.format("Entry not found: %s", context));
    }
}
