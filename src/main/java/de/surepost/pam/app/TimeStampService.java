package de.surepost.pam.app;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class TimeStampService {
    public static final LocalDateTime DEFAULT = LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.UTC);
    private final AtomicReference<LocalDateTime> timestamp;

    public TimeStampService() {
        timestamp = new AtomicReference<>(DEFAULT);
    }

    public void set(LocalDateTime timestamp) {
        if (timestamp == null) {
            return;
        }
        this.timestamp.set(timestamp);
    }

    public LocalDateTime get() {
        return this.timestamp.get();
    }
}
