package de.surepost.pam.app;

import de.surepost.pam.EventType;
import jakarta.persistence.*;

import java.time.Instant;
import java.util.Date;

@Entity
@Table(name = "entry_events")
public class EntryEventEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    public Long id;

    @Column(nullable = false)
    public Date timestamp;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    public EventType event;

    @Column()
    public String username;

    @ManyToOne
    @JoinColumn(nullable = false)
    private EntryEntity entry;

    public EntryEventEntity() {
    }

    public EntryEventEntity(EntryEntity entry, EventType event, String username) {
        this.entry = entry;
        this.event = event;
        this.username = username;
        this.timestamp = Date.from(Instant.now());
    }

    public EntryEventEntity(EntryEntity entry, EventType event) {
        this.entry = entry;
        this.event = event;
        this.username = null;
        this.timestamp = Date.from(Instant.now());
    }
}