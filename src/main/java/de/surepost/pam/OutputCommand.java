package de.surepost.pam;

import de.surepost.pam.app.*;
import de.surepost.pam.output.OutputRecord;
import de.surepost.pam.output.OutputServiceException;
import de.surepost.pam.output.OutputServiceInterface;
import org.springframework.stereotype.Service;

@Service
public class OutputCommand {
    private final EntryRepository entryRepository;
    private final EntryEventRepository entryEventRepository;
    private final OutputServiceInterface outputService;

    public OutputCommand(
            EntryRepository entryRepository,
            EntryEventRepository entryEventRepository,
            OutputServiceInterface outputService
    ) {
        this.entryRepository = entryRepository;
        this.entryEventRepository = entryEventRepository;
        this.outputService = outputService;
    }

    /**
     * Update and export entity to the output service
     */
    public void execute(OutputRecord outputRecord, String username) throws OutputServiceException, EntryNotFoundException {
        EntryEntity entryEntity = entryRepository.findById(outputRecord.id()).orElse(null);
        if (entryEntity == null) {
            throw new EntryNotFoundException(outputRecord.id());
        }
        entryEntity.zip = outputRecord.zip();
        entryEntity.city = outputRecord.city();
        entryEntity.street = outputRecord.street();
        entryEntity.house = outputRecord.house();
        execute(entryEntity, username);
    }

    /**
     * Export entity to the output service
     */
    public void execute(EntryEntity entryEntity, String username) throws OutputServiceException {
        OutputRecord outputRecord = new OutputRecord(entryEntity.id, entryEntity.zip, entryEntity.city, entryEntity.street, entryEntity.house);
        outputService.write(outputRecord);
        entryEventRepository.save(new EntryEventEntity(entryEntity, EventType.EXPORTED, username));
    }
}
