package de.surepost.pam;

import java.util.Objects;

public interface Address {
    String zip();

    String city();

    String street();

    String house();

    /**
     * Exact match
     */
    default boolean same(Address address) {
        if (address == null) {
            return false;
        }
        boolean zipEqual = Objects.equals(this.zip(), address.zip());
        boolean cityEqual = Objects.equals(this.city(), address.city());
        boolean streetEqual = Objects.equals(this.street(), address.street());
        boolean houseEqual = Objects.equals(this.house(), address.house());

        return zipEqual && cityEqual && streetEqual && houseEqual;
    }

    /**
     * Fuzzy match
     */
    default boolean similar(Address address) {
        if (address == null) {
            return false;
        }
        boolean zipEqual = Objects.equals(normalize(this.zip()), normalize(address.zip()));
        boolean cityEqual = Objects.equals(normalize(this.city()), normalize(address.city()));
        boolean streetEqual = Objects.equals(normalize(this.street()), normalize(address.street()));
        boolean houseEqual = Objects.equals(normalize(this.house()), normalize(address.house()));

        return zipEqual && cityEqual && streetEqual && houseEqual;
    }

    static String normalize(String value) {
        if (value == null) {
            return null;
        }
        return value.toLowerCase().trim().replace("strasse", "straße");
    }
}
