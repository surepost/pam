package de.surepost.pam;

public enum EventType {
    IMPORTED,
    VERIFIED,
    EXPORTED
}
