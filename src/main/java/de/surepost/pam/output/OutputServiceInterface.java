package de.surepost.pam.output;

public interface OutputServiceInterface {
    void write(OutputRecord entry) throws OutputServiceException;
}
