package de.surepost.pam.output;

import java.io.IOException;

public class OutputServiceException extends IOException {
    OutputServiceException(String context, Throwable cause) {
        super(String.format("Export failed on: %s", context), cause);
    }
}
