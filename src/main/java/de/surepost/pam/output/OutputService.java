package de.surepost.pam.output;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;

@Service
public class OutputService implements OutputServiceInterface {

    private final String query;
    private final JdbcTemplate jdbcTemplate;

    public OutputService(@Value("${app.recipient.output}") String query, @Qualifier("recipientJdbcTemplate") JdbcTemplate jdbcTemplate) {
        if (query == null || query.isEmpty()) {
            throw new OutputConfigurationException("missing app.recipient.output");
        }
        this.query = query;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void write(OutputRecord outputRecord) throws OutputServiceException {
        if (outputRecord == null) {
            return;
        }
        try {
            jdbcTemplate.update(connection -> {
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, outputRecord.zip());
                preparedStatement.setString(2, outputRecord.city());
                preparedStatement.setString(3, outputRecord.street());
                preparedStatement.setString(4, outputRecord.house());
                preparedStatement.setLong(5, outputRecord.id());
                return preparedStatement;
            });
        } catch (Exception e) {
            throw new OutputServiceException(outputRecord.toString(), e);
        }
    }
}
