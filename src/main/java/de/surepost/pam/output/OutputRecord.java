package de.surepost.pam.output;

import de.surepost.pam.Address;

public record OutputRecord(Long id, String zip, String city, String street, String house) implements Address {
}
