package de.surepost.pam.output;

public class OutputConfigurationException extends IllegalArgumentException {
    OutputConfigurationException(String context) {
        super(String.format("Incorrect output configuration: %s", context));
    }
}
