package de.surepost.pam;

import de.surepost.pam.api.ApiServiceException;
import de.surepost.pam.api.ApiServiceInterface;
import de.surepost.pam.api.ApiVerifyResponse;
import de.surepost.pam.app.*;
import de.surepost.pam.input.InputRecord;
import de.surepost.pam.input.InputServiceException;
import de.surepost.pam.input.InputServiceInterface;
import de.surepost.pam.output.OutputServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class InputCommand {
    private static final Logger LOGGER = LoggerFactory.getLogger(InputCommand.class);
    private final InputServiceInterface inputService;
    private final EntryRepository entryRepository;
    private final EntryEventRepository entryEventRepository;
    private final ApiServiceInterface apiService;
    private final TimeStampService timeStampService;
    private final OutputCommand outputCommand;

    public InputCommand(
            EntryRepository entryRepository,
            EntryEventRepository entryEventRepository,
            ApiServiceInterface apiService,
            InputServiceInterface inputService,
            TimeStampService timeStampService,
            OutputCommand outputCommand
    ) {
        this.inputService = inputService;
        this.entryRepository = entryRepository;
        this.entryEventRepository = entryEventRepository;
        this.apiService = apiService;
        this.timeStampService = timeStampService;
        this.outputCommand = outputCommand;
    }

    /**
     * Import records
     */
    public void execute() {
        LocalDateTime timestamp = timeStampService.get();

        List<InputRecord> inputRecords;
        try {
            inputRecords = inputService.read(timestamp);
        } catch (InputServiceException e) {
            LOGGER.error(e.getMessage(), e);
            return;
        }
        for (InputRecord inputRecord : inputRecords) {
            LOGGER.info(String.format("Found record: %s", inputRecord.id()));

            if (inputRecord.timestamp().isAfter(timestamp)) {
                timestamp = inputRecord.timestamp();
            }

            EntryEntity entryEntity = entryRepository.findById(inputRecord.id()).orElse(null);
            if (entryEntity != null && inputRecord.same(entryEntity)) {
                break;
            }

            if (entryEntity == null) {
                entryEntity = new EntryEntity();
            }
            entryEntity.id = inputRecord.id();
            entryEntity.zip = inputRecord.zip();
            entryEntity.city = inputRecord.city();
            entryEntity.street = inputRecord.street();
            entryEntity.house = inputRecord.house();
            entryEntity.email = inputRecord.email();
            entryEntity.phone = inputRecord.phone();
            entryRepository.save(entryEntity);
            entryEventRepository.save(new EntryEventEntity(entryEntity, EventType.IMPORTED));
            LOGGER.info(String.format("Imported record: %d", entryEntity.id));

            try {
                ApiVerifyResponse apiVerifyResponse = apiService.verify(entryEntity);
                entryEntity.exists = apiVerifyResponse.exists();
                // auto-correction
                if (!entryEntity.exists && apiVerifyResponse.similar(entryEntity)) {
                    entryEntity.exists = true;
                    entryEntity.zip = apiVerifyResponse.zip();
                    entryEntity.city = apiVerifyResponse.city();
                    entryEntity.street = apiVerifyResponse.street();
                    entryEntity.house = apiVerifyResponse.house();
                }
                entryRepository.save(entryEntity);
                entryEventRepository.save(new EntryEventEntity(entryEntity, EventType.VERIFIED));
                LOGGER.info(String.format("Checked record %d: %s", entryEntity.id, entryEntity.exists));
            } catch (ApiServiceException e) {
                LOGGER.error(e.getMessage(), e);
                return;
            }

            if (entryEntity.exists) {
                try {
                    outputCommand.execute(entryEntity, null);
                    LOGGER.info(String.format("Exported record: %d", entryEntity.id));
                } catch (OutputServiceException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        timeStampService.set(timestamp);
    }
}
