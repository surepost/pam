package de.surepost.pam.input;

import java.time.LocalDateTime;
import java.util.List;

public interface InputServiceInterface {
    List<InputRecord> read(LocalDateTime timestamp) throws InputServiceException;
}
