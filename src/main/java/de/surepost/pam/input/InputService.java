package de.surepost.pam.input;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class InputService implements InputServiceInterface {

    private final String query;
    private final JdbcTemplate jdbcTemplate;

    public InputService(@Value("${app.recipient.input}") String query, @Qualifier("recipientJdbcTemplate") JdbcTemplate jdbcTemplate) {
        if (query == null || query.isEmpty()) {
            throw new InputConfigurationException("missing app.recipient.input");
        }
        this.query = query;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<InputRecord> read(LocalDateTime timestamp) throws InputServiceException {
        if (timestamp == null) {
            throw new InputServiceException("nullable argument");
        }
        try {
            return jdbcTemplate.query(query, (rs, rowNum) -> new InputRecord(
                    rs.getLong("id"),
                    rs.getTimestamp("timestamp").toLocalDateTime(),
                    rs.getString("zip"),
                    rs.getString("city"),
                    rs.getString("street"),
                    rs.getString("house"),
                    rs.getString("phone"),
                    rs.getString("email")
            ), timestamp);
        } catch (Exception e) {
            throw new InputServiceException(e.getMessage(), e);
        }
    }
}