package de.surepost.pam.input;

import de.surepost.pam.Address;

import java.time.LocalDateTime;

public record InputRecord(Long id, LocalDateTime timestamp, String zip, String city, String street, String house, String phone, String email) implements Address {
}
