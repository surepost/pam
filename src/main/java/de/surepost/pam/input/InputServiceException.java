package de.surepost.pam.input;

import java.io.IOException;

public class InputServiceException extends IOException {
    private static final String PATTERN = "Import failed on: %s";

    InputServiceException(String context) {
        super(String.format(PATTERN, context));
    }

    InputServiceException(String context, Throwable cause) {
        super(String.format(PATTERN, context), cause);
    }
}
