package de.surepost.pam.input;

public class InputConfigurationException extends IllegalArgumentException {
    InputConfigurationException(String context) {
        super(String.format("Incorrect input configuration: %s", context));
    }
}
