package de.surepost.pam;

public class ConfigurationException extends IllegalArgumentException {
    ConfigurationException(String context) {
        super(String.format("Incorrect configuration: %s", context));
    }
}
