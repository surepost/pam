package de.surepost.pam.api;

public class ApiConfigurationException extends IllegalArgumentException {
    ApiConfigurationException(String context) {
        super(String.format("Incorrect API configuration: %s", context));
    }
}
