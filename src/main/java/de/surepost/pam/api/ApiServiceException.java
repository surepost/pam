package de.surepost.pam.api;

import java.io.IOException;

public class ApiServiceException extends IOException {
    private static final String PATTERN = "API failed with: %s";

    ApiServiceException(String context) {
        super(String.format(PATTERN, context));
    }

    ApiServiceException(String context, Throwable cause) {
        super(String.format(PATTERN, context), cause);
    }

    ApiServiceException(Throwable cause) {
        super(cause);
    }
}
