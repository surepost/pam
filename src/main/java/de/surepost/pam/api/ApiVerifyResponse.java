package de.surepost.pam.api;

import de.surepost.pam.Address;

public record ApiVerifyResponse(boolean exists, String zip, String city, String street, String house) implements Address {
}
