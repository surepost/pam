package de.surepost.pam.api;

import de.surepost.pam.Address;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

@Service
public class ApiService implements ApiServiceInterface {
    private static final String SERVER = "https://surepost.de";
    private final WebClient client;
    private final String token;

    public ApiService(@Value("${app.token}") String token, WebClient.Builder builder) {
        if (token == null || token.isEmpty()) {
            throw new ApiConfigurationException("missing app.token");
        }

        this.client = builder.baseUrl(SERVER).build();
        this.token = token;
    }

    @Override
    public ApiVerifyResponse verify(Address address) throws ApiServiceException {
        if (address == null) {
            throw new ApiServiceException("nullable argument");
        }
        try {
            ApiVerifyRequest body = new ApiVerifyRequest(
                    address.zip() != null ? address.zip() : "",
                    address.city() != null ? address.city() : "",
                    address.street() != null ? address.street() : "",
                    address.house() != null ? address.house() : ""
            );
            ApiVerifyResponse response = client
                    .post()
                    .uri("/api/verify/" + token)
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(body)
                    .retrieve()
                    .bodyToMono(ApiVerifyResponse.class)
                    .block();
            if (null == response) {
                throw new Exception(String.format("Empty response from %s", SERVER));
            }
            return response;
        } catch (Exception e) {
            if (e instanceof WebClientResponseException.BadRequest r) {
                throw new ApiServiceException(r.getResponseBodyAsString(), e);
            }
            throw new ApiServiceException(e);
        }
    }
}
