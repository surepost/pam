package de.surepost.pam.api;

import de.surepost.pam.Address;

public interface ApiServiceInterface {
    ApiVerifyResponse verify(Address address) throws ApiServiceException;
}
