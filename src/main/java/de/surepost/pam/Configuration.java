package de.surepost.pam;

import jakarta.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Map;
import java.util.Objects;

@org.springframework.context.annotation.Configuration
@EnableWebSecurity
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class Configuration {
    private final Environment environment;

    public Configuration(Environment environment) {
        this.environment = environment;
    }

    @Bean(name = "entityManagerFactoryBuilder")
    public EntityManagerFactoryBuilder entityManagerFactoryBuilder() {
        Map<String, String> props = Map.of(
                "hibernate.hbm2ddl.auto", Objects.requireNonNull(environment.getProperty("spring.jpa.hibernate.ddl-auto")),
                "hibernate.dialect", Objects.requireNonNull(environment.getProperty("spring.jpa.database-platform")),
                "hibernate.jdbc.lob.non_contextual_creation", "true"
        );
        return new EntityManagerFactoryBuilder(new HibernateJpaVendorAdapter(), props, null);
    }

    @Bean(name = "dataSource")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean
    entityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("dataSource") DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .packages("de.surepost.pam.app")
                .persistenceUnit("app")
                .build();
    }

    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("entityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    @Bean(name = "recipientDataSource")
    @ConfigurationProperties(prefix = "app.recipient")
    public DataSource recipientDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "recipientJdbcTemplate")
    public JdbcTemplate recipientJdbcTemplate(@Qualifier("recipientDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean(name = "recipientEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean
    recipientEntityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("recipientDataSource") DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .packages("de.surepost.pam.input", "de.surepost.pam.output")
                .persistenceUnit("recipient")
                .build();
    }

    @Bean(name = "recipientTransactionManager")
    public PlatformTransactionManager recipientTransactionManager(@Qualifier("recipientEntityManagerFactory") EntityManagerFactory recipientEntityManagerFactory) {
        return new JpaTransactionManager(recipientEntityManagerFactory);
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeHttpRequests()
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();
        return http.build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        if (environment.getProperty("app.username") == null || environment.getProperty("app.username").isEmpty()) {
            throw new ConfigurationException("missing app.username");
        }
        if (environment.getProperty("app.password") == null || environment.getProperty("app.password").isEmpty()) {
            throw new ConfigurationException("missing app.username");
        }

        UserDetails user = User.withDefaultPasswordEncoder()
                .username(environment.getProperty("app.username"))
                .password(environment.getProperty("app.password"))
                .roles("USER_ROLE")
                .build();
        return new InMemoryUserDetailsManager(user);
    }
}
