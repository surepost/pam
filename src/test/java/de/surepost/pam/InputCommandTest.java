package de.surepost.pam;

import de.surepost.pam.api.ApiServiceInterface;
import de.surepost.pam.api.ApiVerifyResponse;
import de.surepost.pam.app.EntryEntity;
import de.surepost.pam.app.EntryEventRepository;
import de.surepost.pam.app.EntryRepository;
import de.surepost.pam.app.TimeStampService;
import de.surepost.pam.input.InputRecord;
import de.surepost.pam.input.InputServiceInterface;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InputCommandTest {

    private EntryRepository entryRepository;
    private EntryEventRepository entryEventRepository;
    private ApiServiceInterface apiService;
    private InputServiceInterface inputService;
    private TimeStampService timeStampService;
    private OutputCommand outputCommand;

    @BeforeEach
    void setUp() {
        entryRepository = Mockito.mock(EntryRepository.class);
        entryEventRepository = Mockito.mock(EntryEventRepository.class);
        apiService = Mockito.mock(ApiServiceInterface.class);
        inputService = Mockito.mock(InputServiceInterface.class);
        timeStampService = new TimeStampService();
        outputCommand = Mockito.mock(OutputCommand.class);
    }

    @Test
    void executeValidAddress() throws Exception {
        when(inputService.read(any(LocalDateTime.class)))
                .thenReturn(List.of(new InputRecord(
                        1L,
                        LocalDateTime.now(),
                        "10178",
                        "Berlin",
                        "Alexanderstraße",
                        "1",
                        null,
                        null
                )));
        when(apiService.verify(any()))
                .thenReturn(new ApiVerifyResponse(
                        true,
                        "10178",
                        "Berlin",
                        "Alexanderstraße",
                        "1"
                ));
        EntryEntity entry = new EntryEntity();
        when(entryRepository.findById(1L)).thenReturn(Optional.of(entry));

        new InputCommand(
                entryRepository,
                entryEventRepository,
                apiService,
                inputService,
                timeStampService,
                outputCommand
        ).execute();
        verify(outputCommand, times(1)).execute(eq(entry), eq(null));
    }

    @Test
    void executeInvalidAddress() throws Exception {
        when(inputService.read(any(LocalDateTime.class)))
                .thenReturn(List.of(new InputRecord(
                        1L,
                        LocalDateTime.now(),
                        "10178",
                        "Berlin",
                        "Alexanderstraße",
                        "1a",
                        null,
                        null
                )));
        when(apiService.verify(any()))
                .thenReturn(new ApiVerifyResponse(
                        false,
                        "10178",
                        "Berlin",
                        "Alexanderstraße",
                        "1"
                ));
        EntryEntity entry = new EntryEntity();
        when(entryRepository.findById(1L)).thenReturn(Optional.of(entry));

        new InputCommand(
                entryRepository,
                entryEventRepository,
                apiService,
                inputService,
                timeStampService,
                outputCommand
        ).execute();
        verify(outputCommand, times(0)).execute(eq(entry), eq(null));
    }

    @Test
    void executeSameAddress() throws Exception {
        when(inputService.read(any(LocalDateTime.class)))
                .thenReturn(List.of(new InputRecord(
                        1L,
                        LocalDateTime.now(),
                        "10178",
                        "Berlin",
                        "Alexanderstraße",
                        "1",
                        null,
                        null
                )));
        EntryEntity entry = new EntryEntity();
        entry.zip = "10178";
        entry.city = "Berlin";
        entry.street = "Alexanderstraße";
        entry.house = "1";
        when(entryRepository.findById(1L)).thenReturn(Optional.of(entry));

        new InputCommand(
                entryRepository,
                entryEventRepository,
                apiService,
                inputService,
                timeStampService,
                outputCommand
        ).execute();
        verify(outputCommand, times(0)).execute(eq(entry), eq(null));
    }

    @Test
    void executeSimilarAddress() throws Exception {
        when(inputService.read(any(LocalDateTime.class)))
                .thenReturn(List.of(new InputRecord(
                        1L,
                        LocalDateTime.now(),
                        "10969",
                        "BERLIN",
                        "Friedrichstrasse",
                        "23b",
                        null,
                        null
                )));
        when(apiService.verify(any()))
                .thenReturn(new ApiVerifyResponse(
                        false,
                        "10969",
                        "Berlin",
                        "Friedrichstraße",
                        "23B"
                ));
        EntryEntity entry = new EntryEntity();
        when(entryRepository.findById(1L)).thenReturn(Optional.of(entry));

        new InputCommand(
                entryRepository,
                entryEventRepository,
                apiService,
                inputService,
                timeStampService,
                outputCommand
        ).execute();
        verify(outputCommand, times(1)).execute(eq(entry), eq(null));
    }
}