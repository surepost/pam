package de.surepost.pam;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AddressTest {

    @ParameterizedTest
    @MethodSource("similarProvider")
    void similar(Address a, Address b, boolean expected) {
        assertEquals(expected, a.similar(b));
    }

    private static Stream<Arguments> similarProvider() {
        return Stream.of(
                Arguments.of(
                        new AnAddress("12345", "City", "Street", "House"),
                        new AnAddress("12345", "City", "Street", "House"),
                        true
                ),
                Arguments.of(
                        new AnAddress("12345", "City", "Strasse", "House"),
                        new AnAddress("12345", "City", "Straße", "House"),
                        true
                ),
                Arguments.of(
                        new AnAddress("", "", "", ""),
                        new AnAddress("", "", "", ""),
                        true
                ),
                Arguments.of(
                        new AnAddress("12345", "City", "Street", "House"),
                        new AnAddress("12345", "city", "street", "house"),
                        true
                ),
                Arguments.of(
                        new AnAddress("12345", "City", "Street", "House"),
                        new AnAddress("  12345  ", "  city  ", "  street  ", "  house  "),
                        true
                )
        );
    }

    record AnAddress(String zip, String city, String street, String house) implements Address {
    }
}