package de.surepost.pam.output;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.SQLWarningException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OutputServiceTest {

    @Test
    void writeThrowsOutputServiceExceptionOnSQLException() {
        JdbcTemplate jdbcTemplate = Mockito.mock(JdbcTemplate.class);
        when(jdbcTemplate.update(any(PreparedStatementCreator.class))).thenThrow(SQLWarningException.class);
        assertThrows(
                OutputServiceException.class,
                () -> new OutputService("property", jdbcTemplate).write(new OutputRecord(1L, "", "", "", ""))
        );
    }

    @Test
    void writeOnNullableArgument() {
        JdbcTemplate jdbcTemplate = Mockito.mock(JdbcTemplate.class);
        assertDoesNotThrow(() -> new OutputService("property", jdbcTemplate).write(null));
    }

    @Test
    void writeThrowsOutputConfigurationException() {
        JdbcTemplate jdbcTemplate = Mockito.mock(JdbcTemplate.class);
        assertThrows(
                OutputConfigurationException.class,
                () -> new OutputService(null, jdbcTemplate)
        );
    }
}