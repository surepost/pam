package de.surepost.pam.api;

import de.surepost.pam.Address;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ApiServiceTest {
    private WebClient client;
    private WebClient.Builder builder;
    private Address address;

    @BeforeEach
    void setUp() {
        client = Mockito.mock(WebClient.class);
        builder = Mockito.mock(WebClient.Builder.class);
        lenient().when(builder.baseUrl(anyString())).thenReturn(builder);
        lenient().when(builder.build()).thenReturn(client);
        address = new Address() {
            @Override
            public String zip() {
                return "";
            }

            @Override
            public String city() {
                return "";
            }

            @Override
            public String street() {
                return "";
            }

            @Override
            public String house() {
                return "";
            }
        };
    }

    @Test
    void verifyThrowsApiServiceExceptionOnBadRequestResponse() {
        var exception = Mockito.mock(WebClientResponseException.BadRequest.class);
        when(exception.getResponseBodyAsString()).thenReturn("json");
        when(client.post()).thenThrow(exception);
        assertThrows(
                ApiServiceException.class,
                () -> new ApiService("property", builder).verify(address)
        );
    }

    @Test
    void verifyThrowsApiServiceExceptionOnEmptyResponse() {
        when(client.post()).thenReturn(null);
        assertThrows(
                ApiServiceException.class,
                () -> new ApiService("property", builder).verify(address)
        );
    }

    @Test
    void verifyThrowsApiServiceExceptionOnNullableArgument() {
        assertThrows(
                ApiServiceException.class,
                () -> new ApiService("property", builder).verify(null)
        );
    }

    @Test
    void verifyThrowsApiConfigurationException() {
        WebClient.Builder builder = Mockito.mock(WebClient.Builder.class);
        assertThrows(
                ApiConfigurationException.class,
                () -> new ApiService(null, builder)
        );
    }
}