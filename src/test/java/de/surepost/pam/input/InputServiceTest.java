package de.surepost.pam.input;

import de.surepost.pam.app.TimeStampService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.SQLWarningException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class InputServiceTest {

    @Test
    void readThrowsInputServiceExceptionOnSQLException() {
        JdbcTemplate jdbcTemplate = Mockito.mock(JdbcTemplate.class);
        when(jdbcTemplate.query(anyString(), any(RowMapper.class), any(LocalDateTime.class))).thenThrow(SQLWarningException.class);
        assertThrows(
                InputServiceException.class,
                () -> new InputService("property", jdbcTemplate).read(TimeStampService.DEFAULT)
        );
    }

    @Test
    void readThrowsInputServiceExceptionOnNullableArgument() {
        JdbcTemplate jdbcTemplate = Mockito.mock(JdbcTemplate.class);
        assertThrows(
                InputServiceException.class,
                () -> new InputService("property", jdbcTemplate).read(null)
        );
    }

    @Test
    void readThrowsInputConfigurationException() {
        JdbcTemplate jdbcTemplate = Mockito.mock(JdbcTemplate.class);
        assertThrows(
                InputConfigurationException.class,
                () -> new InputService(null, jdbcTemplate)
        );
    }
}