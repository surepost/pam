# PAM
SurePost Postal Address Manager is a tool for the verification, correction, and standardization of postal addresses without interfering with existing business processes.

```plantuml
title Workflows
actor User
participant "Postal Address Manager" as PAM #3C7FC0
participant "Recipient database" as DB #3C7FC0
participant "SurePost API" as API #118C4F

... Automatic correction ...
loop every second
    PAM -> DB: import updated addresses
    DB --> PAM: recordset
    note left of PAM: IMPORTED
    PAM -> API: verify
    API --> PAM: suggestion
    note left of PAM: VERIFIED
    PAM <- PAM: match
    PAM -> DB: export existing addresses
    DB --> PAM: set updated
    note left of PAM: EXPORTED
end

... Manual correction ...
User -> PAM: correct address
PAM -> API: verify
API --> PAM: suggestion
PAM --> User: suggestion
note left of PAM: VERIFIED
User -> PAM: approve
PAM -> DB: export existing addresses
DB --> PAM: set updated
note left of PAM: EXPORTED
PAM --> User: set updated
```

## Features
- MSSQL connector
- MySQL connector
- Oracle connector
- PostgreSQL connector
- internal authenticator

## Requirements
- Java 17
- [surepost.de](https://surepost.de) token

## Usage
- download the latest release from [gitlab.com/surepost/pam/-/releases](https://gitlab.com/surepost/pam/-/releases)
- copy `application-<db>.properties` to `application.properties`
- set `app.token` and update other property values according to your setup
- run ```java -jar pam-<version>.jar```
- open [localhost:8000](http://localhost:8000)
- log in using `app.username` and `app.password`

## Development
Directory `db` includes Docker images that help test all database connectors.
- clone the project
- copy `application-<db>.properties` to `application.properties`
- set demo `app.token` copied from [surepost.de](https://surepost.de)
- run `Main` Spring Boot application

## Roadmap
- external authenticators i.e. SMTPs, SurePost etc
- delegate address correction to the recipient
