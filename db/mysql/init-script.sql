SET NAMES 'utf8mb4';
SET CHARACTER SET utf8mb4;

CREATE DATABASE IF NOT EXISTS contacts;
USE contacts;
GRANT ALL PRIVILEGES ON contacts.* TO 'pam'@'%';
FLUSH PRIVILEGES;

CREATE TABLE IF NOT EXISTS customers (
    id INT AUTO_INCREMENT PRIMARY KEY,
    updated_at DATETIME NOT NULL,
    phone VARCHAR(255) NULL,
    email VARCHAR(255) NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
CREATE TABLE IF NOT EXISTS addresses (
    id INT AUTO_INCREMENT PRIMARY KEY,
    updated_at DATETIME NOT NULL,
    zip VARCHAR(255) NOT NULL,
    city VARCHAR(255) NOT NULL,
    street VARCHAR(255) NOT NULL,
    house VARCHAR(255) NULL,
    customer_id INT,
    FOREIGN KEY (customer_id) REFERENCES customers(id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;

INSERT INTO customers (updated_at, phone, email)
VALUES
    ('2022-01-01 12:34:56', '+49123456789', 'customer1@example.com'),
    ('2022-02-02 14:45:30', '+49234567890', 'customer2@example.com'),
    ('2022-03-03 16:22:18', '+49345678901', 'customer3@example.com'),
    ('2022-04-04 18:11:42', '', ''),
    ('2022-05-05 20:05:55', NULL, NULL);
INSERT INTO addresses (updated_at, zip, city, street, house, customer_id)
VALUES
    ('2022-02-01 14:45:30', '10178', 'Berlin', 'Alexanderstraße', '1a', 2),
    ('2022-01-02 12:34:56', '10178', 'Berlin', 'Alexanderstraße', '1', 1),
    ('2022-03-03 16:22:18', '10969', 'BERLIN', 'Friedrichstrasse', '23b', 3),
    ('2022-04-04 18:11:42', '50667', 'Köln', 'Rathausplatz', '', 4),
    ('2022-05-05 20:05:55', '56789', 'Frankfurt am Main', 'Theodor-W.-Adorno-Platz', NULL, 5);
