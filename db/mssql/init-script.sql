CREATE DATABASE contacts;
GO
USE contacts;
GO
CREATE TABLE customers (
   id INT PRIMARY KEY IDENTITY(1,1),
   updated_at DATETIME2 NOT NULL,
   phone NVARCHAR(255) NULL,
   email NVARCHAR(255) NULL
);
GO
CREATE TABLE addresses (
   id INT PRIMARY KEY IDENTITY(1,1),
   updated_at DATETIME2 NOT NULL,
   zip NVARCHAR(255) NOT NULL,
   city NVARCHAR(255) NOT NULL,
   street NVARCHAR(255) NOT NULL,
   house NVARCHAR(255) NULL,
   customer_id INT,
   FOREIGN KEY (customer_id) REFERENCES customers(id)
);
GO
INSERT INTO customers (updated_at, phone, email)
VALUES
    ('2022-01-01 12:34:56', '+49123456789', 'customer1@example.com'),
    ('2022-02-02 14:45:30', '+49234567890', 'customer2@example.com'),
    ('2022-03-03 16:22:18', '+49345678901', 'customer3@example.com'),
    ('2022-04-04 18:11:42', '', ''),
    ('2022-05-05 20:05:55', NULL, NULL);
GO
INSERT INTO addresses (updated_at, zip, city, street, house, customer_id)
VALUES
    ('2022-02-01 14:45:30', '10178', 'Berlin', 'Alexanderstraße', '1a', 2),
    ('2022-01-02 12:34:56', '10178', 'Berlin', 'Alexanderstraße', '1', 1),
    ('2022-03-03 16:22:18', '10969', 'BERLIN', 'Friedrichstrasse', '23b', 3),
    ('2022-04-04 18:11:42', '50667', 'Köln', 'Rathausplatz', '', 4),
    ('2022-05-05 20:05:55', '56789', 'Frankfurt am Main', 'Theodor-W.-Adorno-Platz', NULL, 5);
GO
