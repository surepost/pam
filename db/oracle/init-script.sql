ALTER SESSION SET CONTAINER=FREEPDB1;
GRANT CONNECT, RESOURCE TO contacts;

CREATE TABLE contacts.customers (
   id NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
   updated_at TIMESTAMP NOT NULL,
   phone VARCHAR2(255),
   email VARCHAR2(255)
);
CREATE TABLE contacts.addresses (
   id NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
   updated_at TIMESTAMP NOT NULL,
   zip VARCHAR2(255) NOT NULL,
   city VARCHAR2(255) NOT NULL,
   street VARCHAR2(255) NOT NULL,
   house VARCHAR2(255),
   customer_id NUMBER,
   CONSTRAINT fk_customer_id FOREIGN KEY (customer_id) REFERENCES contacts.customers(id)
);

INSERT INTO contacts.customers (updated_at, phone, email)
VALUES
    (TO_TIMESTAMP('2022-01-01 12:34:56', 'YYYY-MM-DD HH24:MI:SS'), '+49123456789', 'customer1@example.com'),
    (TO_TIMESTAMP('2022-02-02 14:45:30', 'YYYY-MM-DD HH24:MI:SS'), '+49234567890', 'customer2@example.com'),
    (TO_TIMESTAMP('2022-03-03 16:22:18', 'YYYY-MM-DD HH24:MI:SS'), '+49345678901', 'customer3@example.com'),
    (TO_TIMESTAMP('2022-04-04 18:11:42', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL),
    (TO_TIMESTAMP('2022-05-05 20:05:55', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);

INSERT INTO contacts.addresses (updated_at, zip, city, street, house, customer_id)
VALUES
    (TO_TIMESTAMP('2022-02-01 14:45:30', 'YYYY-MM-DD HH24:MI:SS'), '10178', 'Berlin', 'Alexanderstraße', '1a', 2),
    (TO_TIMESTAMP('2022-01-02 12:34:56', 'YYYY-MM-DD HH24:MI:SS'), '10178', 'Berlin', 'Alexanderstraße', '1', 1),
    (TO_TIMESTAMP('2022-03-03 16:22:18', 'YYYY-MM-DD HH24:MI:SS'), '10969', 'BERLIN', 'Friedrichstrasse', '23b', 3),
    (TO_TIMESTAMP('2022-04-04 18:11:42', 'YYYY-MM-DD HH24:MI:SS'), '50667', 'Köln', 'Rathausplatz', NULL, 4),
    (TO_TIMESTAMP('2022-05-05 20:05:55', 'YYYY-MM-DD HH24:MI:SS'), '56789', 'Frankfurt am Main', 'Theodor-W.-Adorno-Platz', NULL, 5);
